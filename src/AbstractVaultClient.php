<?php declare(strict_types=1);
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace vsitnikov\Vault;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\GuzzleException;

use vsitnikov\Vault\Exceptions\VaultInitializationException;

/**
 * Vault class
 *
 * @author    Vyacheslav Sitnikov <vsitnikov@gmail.com>
 * @version   1.0
 * @package   vsitnikov\Vault
 * @copyright Copyright (c) 2019
 */
abstract class AbstractVaultClient
{
    public const HTTP_TIMEOUT = 30;
    
    public const RESPONSE_NONE = 0x00;
    public const RESPONSE_RAW = 0x01;
    public const RESPONSE_PAYLOAD = 0x02;
    public const RESPONSE_KEY = 0x04;
    public const RESPONSE_KEY_PATH = 0x08;
    public const RESPONSE_CODE = 0x10;
    public const RESPONSE_REASON = 0x20;
    
    public const RESPONSE_DEBUG = 0x80;
    public const RESPONSE_ALL = 0xFF & ~self::RESPONSE_DEBUG;
    
    public const SEARCH_MESSAGES = 0x01;
    public const SEARCH_DIRS = 0x02;
    public const SEARCH_ALL = self::SEARCH_MESSAGES | self::SEARCH_DIRS;
    
    public const AUTH_TOKEN = "token";
    public const AUTH_USERPASS = "userpass";
    public const AUTH_LDAP = "ldap";
    public const AUTH_APPROLE = "approle";
    
    private static $auth_method = self::AUTH_TOKEN;
    private static $auth_token = null;
    private static $auth_token_expire = null;
    private static $auth_username = null;
    private static $auth_password = null;
    private static $auth_role_id = null;
    private static $auth_secret_id = null;
    
    protected static $default_params = [
        "address"      => null,
        "secret"       => null,
        "default_path" => "",
        "http_options" => [],
        "ssl_cert"     => true,
        "client"       => null,
        "proto"        => "https",
        "server"       => null,
        "port"         => 8200,
        "response"     => self::RESPONSE_ALL,
    ];
    
    protected static $params = null;
    
    /**
     *  Return new class instance
     *
     * @param array|null $params             Initialization data, see init()
     * @param bool|null  $use_default_params [optional] [false] Create class based on default parameters
     *
     * @return AbstractVaultClient
     * @see AbstractVaultClient::init()
     *
     */
    public static function new(?array $params = [], ?bool $use_default_params = false): AbstractVaultClient
    {
        $params = array_merge($use_default_params ? self::$default_params : static::$params ?? [], $params ?? []);
        $new_class_type = "vaultTmpClass".uniqid();
        return eval("class {$new_class_type} extends ".__CLASS__." { protected static \$params = null; }; return new {$new_class_type}(\$params);//{$params}");
    }
    
    /**
     * Class constructor, all data passed to init method
     *
     * @param mixed ...$args
     *
     * @throws VaultInitializationException
     * @see AbstractVaultClient::init
     */
    public function __construct(...$args)
    {
        self::init(...$args);
    }
    
    /**
     *  Class initialization
     *
     * @param array $params <pre>
     *
     * Initialization data
     * server       VAULT server address
     * port         [optional] vault server port, default 8200
     * proto        [optional] connection protocol, default https
     * ssl_cert     [optional] path to ssl cert, false for ignore ssl errors
     * secret       name of secret database
     * default_path [optional] default no
     * http_options [optional] curl initialization parameters
     * response     [optional] response fields, default 127 ( vault::RESPONSE_ALL )
     * auth         array for auth:
     *                 method    - vault::AUTH_TOKEN, vault::AUTH_USERPASS, vault::AUTH_LDAP, vault::AUTH_APPROLE
     *                 token     - token value for vault::AUTH_TOKEN method
     *                 username  - username for vault::AUTH_USERPASS or vault::AUTH_LDAP methods
     *                 password  - password for vault::AUTH_USERPASS or vault::AUTH_LDAP methods
     *                 role_id   - role id for vault::AUTH_APPROLE method
     *                 secret_id - secret id for vault::AUTH_APPROLE method
     * </pre>
     *
     * @throws VaultInitializationException
     */
    public static function init(?array $params = [])
    {
        if (is_null(static::$params)) {
            static::$params = self::$default_params;
        }
        
        $new_params = self::checkInitParams($params);
        $new_params['address'] = "{$new_params['proto']}://{$new_params['server']}:{$new_params['port']}";
        self::setParams($new_params);
        if (isset($params['auth'])) {
            self::setAuth($params);
        }
    }
    
    /**
     *  Check initialization parameters
     *
     * @param array $params Input params
     * @param bool  $silent [optional] Don't throw exception, default true
     *
     * @return array|false false on fail or array, contains all class properties (internal and getting)
     * @throws VaultInitializationException
     */
    protected static function checkInitParams(array $params, bool $silent = false)
    {
        //  Fill the ONLY EXISTS parameters with the getting values
        $local_vars = self::getParams();
        foreach ($params as $key => $value) {
            if (array_key_exists($key, $local_vars)) {
                $local_vars[$key] = $value;
            }
        }
        
        //  Check getting parameters
        if (is_null($local_vars['server'])) {
            if (!$silent) {
                throw new VaultInitializationException('"server" must be specified');
            } else {
                return false;
            }
        }
        if (!is_numeric($local_vars['port']) or $local_vars['port'] < 1 || $local_vars['port'] > 65534) {
            if (!$silent) {
                throw new VaultInitializationException('"port" must be number in range 1-65534');
            } else {
                return false;
            }
        }
        if (!in_array($local_vars['proto'], ['http', 'https'])) {
            if (!$silent) {
                throw new VaultInitializationException('"proto" must be "https" or "http" only');
            } else {
                return false;
            }
        }
        if (is_null($local_vars['secret'])) {
            if (!$silent) {
                throw new VaultInitializationException('"secret" must be specified');
            } else {
                return false;
            }
        }
        return $local_vars;
    }
    
    /**
     *  Connect to Vault
     *
     * @param array|null $options    [optional] Additional connection options
     * @param bool|null  $persistent [optional] [false] Store additional options for default connections
     *
     * @return Client
     */
    private static function getClient(?array $options = null, ?bool $persistent = false): Client
    {
        //  Return existing client, if no more additional options
        if (is_null($options) && self::issetParam('client')) {
            return self::getParam("client");
        }
        
        //  Set additional options if needed
        $http_options = self::getParam("http_options");
        if (is_array($options)) {
            $http_options = array_merge($http_options, $options);
        }
        $http_options['timeout'] = $http_options['timeout'] ?? self::HTTP_TIMEOUT;
        
        $client = new Client([array_diff($http_options, [null])]);
        return !$persistent ? $client : self::setParam('client', $client);
    }
    
    /**
     *  Get parameter by name
     *
     * @param string $name Name of parameter
     *
     * @return mixed|null Parameter value or null if parameter not exists
     */
    protected static function getParam(string $name)
    {
        if (is_array(static::$params) && array_key_exists($name, static::$params ?? [])) {
            return static::$params[$name];
        }
        return null;
    }
    
    /**
     *  Get array of all parameters
     *
     * @return array
     */
    public static function getParams(): array
    {
        return static::$params ?? [];
    }
    
    /**
     * Set parameter value
     *
     * @param string $name  Parameter name
     * @param mixed  $value Parameter value
     *
     * @return mixed
     */
    protected static function setParam(string $name, $value)
    {
        return static::$params[$name] = $value;
    }
    
    /**
     *  Store all parameters
     *
     * @param array $value Array of all parameters
     *
     * @return array
     */
    protected static function setParams(array $value): array
    {
        return static::$params = $value;
    }
    
    /**
     *  Check of parameter exists by name
     *
     * @param string $name Name of parameter
     *
     * @return bool
     */
    protected static function issetParam(string $name): bool
    {
        return isset(static::$params[$name]);
    }
    
    /**
     * Delete parameter by name
     *
     * @param string $name Name of parameter
     */
    protected static function unsetParam(string $name): void
    {
        unset(static::$params[$name]);
    }
    
    /**
     *  setDefaultPath
     *
     * @param string $path Default path
     *
     * @return mixed Value
     * @throws VaultInitializationException
     */
    public static function setDefaultPath($path)
    {
        self::checkInitParams([]);
        return self::setParam('default_path', trim($path, "/"));
    }
    
    /**
     *  Request to VAULT
     *
     * @param array|null $request_params <pre>
     *
     * Request parameters:
     * string type         Type request (POST, GET, PUT, etc...).
     * string url          [optional] VAULT api address, may be complete or relative, see "Generate URL address" below
     * string api          [optional] [v1] Version API for operation
     * array  payload      [optional] Payload
     * array  http_options [optional] HTTP client parameters
     * string operation    [optional] Operation name
     * array  params       [optional] Initialization parameters
     * </pre>
     *
     * @return array
     * <pre>
     * Result array. Format:
     * bool         ['result']  Result operation (true if success, false if failure)
     * string       ['raw']     Raw body, depending on the constant self::DEFAULT_RESPONSE_RAW
     * array|object ['payload'] Payload response, must be an object or array, depending on the constant self::DEFAULT_RESPONSE_ARRAY
     * int          ['code']    Response code, depending on the constant self::DEFAULT_RESPONSE_CODE
     * string       ['reason']  Message of response code, depending on the constant self::DEFAULT_RESPONSE_REASON
     * string       ['debug']   Debug information, depending on the constant self::DEFAULT_RESPONSE_DEBUG
     * array        ['params']  Current query parameters
     * </pre>
     * @throws VaultInitializationException
     */
    protected static function request(array $request_params = [])
    {
        //  Initialization
        $type = $request_params['type'] ?? "POST";
        $url = $request_params['url'];
        $api = $request_params['api'] == "v2" ? "v2" : "v1";
        $payload = $request_params['payload'];
        $operation = $request_params['operation'] ?? "data";
        $http_options = $request_params['http_options'];
        $params = array_merge(self::getParams(), $request_params['params'] ?? []);
        $client = self::getClient($http_options);
        
        //  Generate URL address:
        //  - default api address if $url not set
        //  - $url as address if url is fill (begin from http)
        //  - add $url to default path if default path exists and url not begin from slash
        //  - add $url to default api address if other
        //  
        $secret = $operation != "auth" ? "{$params['secret']}/" : "";
        $default_path = $operation != "auth" ? "{$params['default_path']}/" : "";
        if (is_null($url) || $url[0] == "/") {
            $url = $params['address']."/{$api}/{$secret}{$operation}".($url ?? "");
        } elseif (substr($url, 0, 4) != "http") {
            $url = $params['default_path'] != "" ? "{$params['address']}/{$api}/{$secret}{$operation}/{$default_path}{$url}" : "{$params['address']}/{$api}/{$secret}{$operation}/{$url}";
        }
        
        //  Prepare data for send
        $data = ['json' => $payload];
        if (is_string(self::$auth_token)) {
            $data['headers'] = ["X-Vault-Token" => self::$auth_token];
        }
        if ($params['response'] & self::RESPONSE_DEBUG) {
            $debug = fopen("php://temp", "r+");
            $data['debug'] = $debug;
        } else {
            $debug = false;
        }
        
        //  Prepare SSL cert or ignore
        $data['verify'] = $params['ssl_cert'];
        
        //  Request
        $result = ['params' => $params];
        $response = null;
        try {
            $response = $client->request($type, $url, $data);
            $result['code'] = (int)$response->getStatusCode();
            $result['reason'] = $response->getReasonPhrase();
        } catch (GuzzleException $e) {
            if ($e instanceof RequestException && $e->hasResponse()) {
                $response = $e->getResponse();
                $result['code'] = (int)$response->getStatusCode();
                $result['reason'] = $response->getReasonPhrase();
            }
        } catch (\Exception $e) {
            $result['code'] = $e->getCode();
            $result['reason'] = $e->getMessage();
        } finally {
            if (false !== $debug) {
                $result['url'] = $url;
                $result['type'] = $type;
                rewind($debug);
                $result['debug'] = stream_get_contents($debug);
                $result['debug'] = preg_replace('/^\* Expire in \d+.*$\n/m', "", $result['debug']);
                fclose($debug);
                $result['json'] = json_encode($data['json'], JSON_UNESCAPED_UNICODE);
                $result['ssl_cert'] = $data['verify'];
            }
            if (is_null($response)) {
                //print_r($result);
                return ["result" => false];
            }
        }
        
        //  Prepare response
        $result['raw'] = $response->getBody()->getContents();
        $result['payload'] = json_decode($result['raw'], true, 512, JSON_BIGINT_AS_STRING);
        $result['result'] = json_last_error() == JSON_ERROR_NONE;
        
        if ($result['result']) {
            if (isset($result['payload']['errors'])) {
                $result['reason'] = implode(", ", $result['payload']['errors']);
                $result['result'] = false;
            }
        }
        
        //print_r($result);
        
        //  Token not set or expire
        if (in_array($result['reason'], ["missing client token", "permission denied"]) && !$request_params['vault_second_chance']) {
            self::getAuthToken();
            $request_params['vault_second_chance'] = true;
            return self::request($request_params);
        }
        return $result;
    }
    
    /**
     *  Set auth params
     *
     * @param array|null $params Auth params
     *
     * @return bool Result
     * @throws VaultInitializationException
     */
    public static function setAuth(?array $params = null): bool
    {
        $params = $params ?? [];
        $modes = [self::AUTH_TOKEN, self::AUTH_USERPASS, self::AUTH_APPROLE, self::AUTH_LDAP];
        if (!in_array($params['auth']['method'], $modes)) {
            throw new VaultInitializationException('mode will be set as: '.implode(",", $modes));
        }
        $set = false;
        foreach ($params['auth'] as $key => $value) {
            $name = "auth_{$key}";
            if (property_exists(self::class, $name)) {
                $set = true;
                self::$$name = $value;
            }
        }
        return $set;
    }
    
    /**
     *  Create token from current auth params
     *
     * @param array|null $params
     *
     * @return array
     * @throws VaultInitializationException
     */
    public static function getAuthToken(?array $params = null): array
    {
        $vault_params = self::checkInitParams($params ?? []);
        $result = [];
        switch ($vault_params['method'] ?? self::$auth_method) {
            case self::AUTH_TOKEN:
                self::$auth_token = $vault_params['token'];
                break;
            case self::AUTH_USERPASS:
                $payload = ["password" => self::$auth_password];
                $result = self::request(
                    [
                        'type'      => "POST",
                        'operation' => "auth",
                        "url"       => "userpass/login/".self::$auth_username,
                        'payload'   => $payload,
                        'params'    =>
                            $params
                    ]
                );
                $token = $result['payload']['auth']['client_token'];
                if ($result['code'] == 200 && $token != "") {
                    self::$auth_token = $token;
                }
            case self::AUTH_LDAP:
                $payload = ["password" => self::$auth_password];
                $result = self::request(
                    [
                        'type'      => "POST",
                        'operation' => "auth",
                        "url"       => "ldap/login/".self::$auth_username,
                        'payload'   => $payload,
                        'params'    =>
                            $params
                    ]
                );
                $token = $result['payload']['auth']['client_token'];
                if ($result['code'] == 200 && $token != "") {
                    self::$auth_token = $token;
                }
            case self::AUTH_APPROLE:
                $payload = ["role_id" => self::$auth_role_id, "secret_id" => self::$auth_secret_id];
                $result = self::request(['type' => "POST", 'operation' => "auth", "url" => "approle/login", 'payload' => $payload, 'params' => $params]);
                $token = $result['payload']['auth']['client_token'];
                if ($result['code'] == 200 && $token != "") {
                    self::$auth_token = $token;
                }
            default:
                return $result;
        }
        $result['result'] = $result['code'] == 200 && isset($result['payload']['data']);
        if ($result['result']) {
            $result['value'] = $result['payload']['data'];
        }
        return self::stripResponse($result);
    }
    
    /**
     *  Get KV Engine config
     *
     * @param array|null $params [optional] Initialization parameters
     *
     * @return array Result array
     * @throws VaultInitializationException
     */
    public static function readConfiguration(?array $params = null): array
    {
        $vault_params = self::checkInitParams($params ?? []);
        $result = self::request(['type' => "GET", 'operation' => "config", "params" => $vault_params]);
        $result['result'] = $result['code'] == 200 && isset($result['payload']['data']);
        if ($result['result']) {
            $result['value'] = $result['payload']['data'];
        }
        return self::stripResponse($result);
    }
    
    /**
     *  Check VAULT connect
     *
     * @param array|null $params [optional] Initialization parameters
     *
     * @return array Result array
     * @throws VaultInitializationException
     */
    public static function healthCheck(?array $params = null): array
    {
        $vault_params = self::checkInitParams($params ?? []);
        $vault_params['secret'] = "sys";
        $result = self::request(['type' => "GET", 'operation' => "health", "params" => $vault_params]);
        $result['result'] = in_array($result['code'], [200]);
        if ($result['result']) {
            $result['value'] = $result['payload'];
        }
        return self::stripResponse($result);
    }
    
    /**
     *  Setting/Changing values of a key
     *
     * @param string     $path   Key path
     * @param array      $value  Values
     * @param array|null $params [optional] Initialization parameters
     *
     * @return array Result array
     * @throws VaultInitializationException
     */
    public static function set(string $path, array $value, ?array $params = null): array
    {
        self::checkInitParams($params ?? []);
        $path = rtrim($path, "/");
        $payload = ["data" => $value];
        $result = self::request(['type' => "POST", 'operation' => "data", "url" => $path, 'payload' => $payload, 'params' => $params]);
        $result['result'] = in_array($result['code'], [200]);
        if ($result['result']) {
            $result['value'] = $result['payload']['data'];
        }
        return self::stripResponse($result);
    }
    
    /**
     *  Get value of a key
     *
     * @param string     $path   Key
     * @param array|null $params [optional] Initialization parameters
     *
     * @return array Result array + ['value'] - key value
     * @throws VaultInitializationException
     */
    public static function get(string $path, ?array $params = null): array
    {
        self::checkInitParams($params ?? []);
        $path = rtrim($path, "/");
        $result = self::request(['type' => "GET", 'operation' => "data", "url" => $path, 'params' => $params]);
        $result['result'] = $result['code'] == 200 && isset($result['payload']['data']['data']);
        if ($result['result']) {
            $result['metadata'] = $result['payload']['data']['metadata'];
            $result['value'] = $result['payload']['data']['data'];
        }
        return self::stripResponse($result);
    }
    
    /**
     *  Delete value of a key
     *
     * @param string     $path   Path to deleting key
     * @param array|null $params [optional] Initialization parameters
     *
     * @return array Result array
     * @throws VaultInitializationException
     */
    public static function delete(string $path, ?array $params = null): array
    {
        self::checkInitParams($params ?? []);
        $get = self::get($path, $params);
        $path = rtrim($path, "/");
        $result = self::request(['type' => "DELETE", 'operation' => "metadata", "url" => $path, 'api' => 'v1', 'params' => $params]);
        $result = ["result" => $result['code'] == 204];
        return self::stripResponse($result);
    }
    
    /**
     *  List
     *
     * @param string     $path   Path to scan
     * @param bool|null  $recursive
     * @param array|null $params [optional] Initialization parameters
     *
     * @return array Result array
     * @throws VaultInitializationException
     */
    public static function readDirRecursive(string $path, ?bool $recursive = null, ?array $params = null): array
    {
        self::checkInitParams($params ?? []);
        $recursive = $recursive ?? true;
        if ($path != "" && $path[-1] != "/")
            $path = $path."/";
        $result = self::request(['type' => "LIST", 'operation' => "metadata", "url" => $path, 'api' => 'v1', 'params' => $params]);
        if (isset($result['payload']['data']['keys'])) {
            $result['value'] = [];
            for ($i = 0; $i < sizeof($result['payload']['data']['keys']); $i++) {
                $path_key = $result['payload']['data']['keys'][$i];
                $key = rtrim($result['payload']['data']['keys'][$i], "/");
                $new_path = $path != "" ? "{$path}{$key}" : $key;
                if ($key != $path_key) {
                    if ($recursive) {
                        $dir = self::readDirRecursive($new_path, $recursive, $params);
                        $result['value'][$key] = $dir['value'];
                    } else {
                        $result['value'][$key] = "dir";
                    }
                } else {
                    $result['value'][$key] = self::get($new_path)['value'];
                }
            }
        }
        return $result;
    }
    
    /**
     *  Read value from directory
     *
     * @param string     $dir_path Path to the directory from which the value will be read
     * @param array|null $params   [optional] Initialization parameters
     *
     * @return array Result array + ['value'] - $result['payload']['node']['nodes']
     * @throws VaultInitializationException
     * @see AbstractVaultClient::readDirRecursive()
     *
     */
    public static function readDir(string $dir_path, ?array $params = null)
    {
        return self::readDirRecursive($dir_path, false, $params);
    }
    
    /**
     *  Check if the path is a directory (v2 protocol only)
     *
     * @param string|null $path   Checking path
     * @param array|null  $params [optional] Initialization parameters
     *
     * @return bool
     * @throws VaultInitializationException
     */
    public static function isDir(?string $path, ?array $params = null): bool
    {
        self::checkInitParams($params ?? []);
        $result = self::get($path, ["response" => self::RESPONSE_CODE])['code'];
        return $result !== 200;
    }
    
    /**
     *  Cut output array
     *
     * @param array $response Response with query settings
     *
     * @return array Result array - cut fields
     */
    protected static function stripResponse(array $response): array
    {
        if (!isset($response['params']['response'])) {
            return $response;
        }
        $exclude = ['params'];
        if (!($response['params']['response'] & self::RESPONSE_RAW)) {
            $exclude[] = 'raw';
        }
        if (!($response['params']['response'] & self::RESPONSE_PAYLOAD)) {
            $exclude[] = 'payload';
        }
        if (!($response['params']['response'] & self::RESPONSE_KEY)) {
            $exclude[] = 'key';
        }
        if (!($response['params']['response'] & self::RESPONSE_KEY_PATH)) {
            $exclude[] = 'key_path';
        }
        if (!($response['params']['response'] & self::RESPONSE_CODE)) {
            $exclude[] = 'code';
        }
        if (!($response['params']['response'] & self::RESPONSE_REASON)) {
            $exclude[] = 'reason';
        }
        if (!($response['params']['response'] & self::RESPONSE_DEBUG)) {
            $exclude[] = 'debug';
        }
        return array_diff_key($response, array_flip($exclude));
    }
    
}
