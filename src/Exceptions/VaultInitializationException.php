<?php

namespace vsitnikov\Vault\Exceptions;

/**
 * Class VaultInitializationException
 *
 * @package vsitnikov\Vault\Exceptions
 */
class VaultInitializationException extends VaultException
{
}
