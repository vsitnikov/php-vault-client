<?php

namespace vsitnikov\Vault\Exceptions;

use Exception;

/**
 * Class VaultException
 *
 * @package vsitnikov\Vault\Exceptions
 */
class VaultException extends Exception
{
}
