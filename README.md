####Attention! This project is in development, do not use it, everything can change completely.

##Create class
#####Initialization parameters
```php
$params = [
    "server" => "server.ru",
    "port"   => 8200,
    "secret" => "secret",
    "auth" => [
        "method"   => vault::AUTH_USERPASS,
        "username" => "my_login",
        "password" => "my_password",
    ]
];
```
#####Initialize abstract class
```php
vault::init($params);
```

#####Create new class identical on abstract class settings
```php
$vault_class1 = vault::new();
```

#####Modify settings abstract class
```php
vault::init(['port' => 8200]);
```

###Multiple instances
#####Create new class with new settings, based on abstract class settings
```php
$vault_class2 = vault::new(['server' => "newserver.ru"], true);
```

#####Create new class with new settings, based on $vault_class1 settings
```php
$vault_class3 = $vault_class1::new(['server' => "oldserver.ru"]);
```

or

```php
$vault_class3 = $vault_class1->new(['server' => "oldserver.ru"]);
```

###Usage
#####Set default path
```php
vault::setDefaultPath("/ps/data/apps/mondb/unit_test");
```

#####Get data from full path (path begin from slash)
```php
$result = vault::get("/data/where/read/test_data");
```

#####Get data from relative (for default path or full path if default path not exists) path (path NOT begin from slash or http)
```php
$result = vault::get("test_data");
```

##### Get data with personal query settings
```php
$result = vault::get("test", ['response' => vault::RESPONSE_CODE | vault::RESPONSE_DEBUG]);
```

