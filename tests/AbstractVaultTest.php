<?php declare(strict_types=1);

/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

use PHPUnit\Framework\TestCase;
use vsitnikov\Vault\AbstractVaultClient as vault;
use vsitnikov\Vault\Exceptions\VaultInitializationException;

/**
 * VAULT class
 *
 * @author    Vyacheslav Sitnikov <vsitnikov@gmail.com>
 * @version   1.0
 * @package   vsitnikov\Vault
 * @copyright Copyright (c) 2019
 */
final class AbstractVaultTest extends TestCase
{
    /**
     * VAULT will throw initialization exception
     *
     * @expectedException vsitnikov\Vault\Exceptions\VaultInitializationException
     */
    public function testBeforeInitialization()
    {
        vault::get("dummy");
    }
    
    /**
     * Tests Initialization
     */
    public function testInitialization()
    {
        //  Import the initialized parameters and add them with a deliberately false parameter
        global $global_options;
        $global_options['dummy'] = "foo";
        $global_options['response'] = ($global_options['response'] ?? vault::RESPONSE_NONE) | vault::RESPONSE_PAYLOAD | vault::RESPONSE_CODE;
        
        //  Initialize an abstract class, there should be no false parameter
        vault::init($global_options);
        $this->assertArrayNotHasKey("dummy", vault::getParams());
        $this->assertEquals($global_options['server'], vault::getParams()['server']);
    }
    
    /**
     * Test new instances
     */
    public function testCheckCreateNewInstances()
    {
        $this->assertInstanceOf(vault::class, $vaultClass1 = vault::new());
        $this->assertEquals(vault::getParams(), $vaultClass1::getParams());
        $vaultClass1::init(["server" => "dummy"]);
        $this->assertNotEquals(vault::getParams(), $vaultClass1::getParams());
        $vaultClass2 = vault::new(["server" => "dummy"]);
        $this->assertEquals($vaultClass1::getParams(), $vaultClass2::getParams());
        $vaultClass2::init(["port" => "1"]);
        $this->assertNotEquals($vaultClass1::getParams(), $vaultClass2::getParams());
    }
    
    /**
     * Test health check
     */
    public function testHealthCheck()
    {
        $result = vault::healthCheck(["response" => vault::RESPONSE_CODE]);
        $this->assertIsArray($result, "Incorrect result");
        $this->assertArrayHasKey("code", $result, "Result without code");
        $this->assertEquals(200, $result['code'], "Vault invalid state");
    }
    
    /**
     * Test read configuration
     */
    public function testReadConfiguration()
    {
        $result = vault::readConfiguration();
        $this->assertIsArray($result, "Incorrect result");
        $this->assertEquals($result['result'], true);
        $this->assertArrayHasKey("payload", $result, "Result without content");
    }
    
    /**
     * Test write value
     */
    public function testSetValue()
    {
        vault::setDefaultPath("data/zopa");
        $result = vault::set("mopa/zeta", ["first" => "one"]);
        $this->assertIsArray($result, "Incorrect result");
        $this->assertEquals($result['result'], true);
        $this->assertArrayHasKey("payload", $result, "Result without content");
    }
    
    /**
     * Test read value
     */
    public function testGetValue()
    {
        $result = vault::get("data/zopa/mopa/zeta");
        $this->assertIsArray($result, "Incorrect result");
        $this->assertEquals($result['result'], false);
        $result = vault::get("/data/zopa/mopa/zeta");
        $this->assertEquals($result['result'], true);
        $this->assertEquals($result['value']['first'], "one");
    }
    
    /**
     * Test read value
     */
    public function testReadDir()
    {
        //  Use relative path
        $result = vault::readDir("data/zopa/mopa/zeta");
        $this->assertIsArray($result, "Incorrect result");
        $this->assertEquals($result['result'], false);
        
        //  Use absolute path
        $result = vault::readDir("/data/zopa/mopa/zeta");
        $this->assertEquals($result['result'], false);
        
        
        $result = vault::readDir("data/zopa/mopa");
        $this->assertEquals($result['result'], false);
        
        $result = vault::readDir("/data/zopa/mopa");
        $this->assertEquals($result['result'], true);
        $this->assertArrayHasKey("zeta", $result['value'], "Incorrect list");
    }
    
    /**
     * Test read value
     */
    public function testReadDirRecursive()
    {
        //  Read all data recursive, include values
        $result = vault::readDirRecursive("/", true);
        $this->assertIsArray($result, "Incorrect result");
        $this->assertArrayHasKey("data", $result['value']);
        $this->assertArrayHasKey("zopa", $result['value']['data']);
        $this->assertArrayHasKey("mopa", $result['value']['data']['zopa']);
        $this->assertArrayHasKey("zeta", $result['value']['data']['zopa']['mopa']);
        $this->assertArrayHasKey("first", $result['value']['data']['zopa']['mopa']['zeta']);
        $this->assertEquals("one", $result['value']['data']['zopa']['mopa']['zeta']['first']);
        
        //  Read last dir non recursive
        $result = vault::readDirRecursive("/data/zopa/mopa/", false);
        $this->assertArrayHasKey("zeta", $result['value']);
        $this->assertArrayHasKey("first", $result['value']['zeta']);
        $this->assertEquals("one", $result['value']['zeta']['first']);
    }
    
    /**
     * Test isDir
     */
    public function testIsDir()
    {
        $result = vault::isDir("/data/zopa/mopa/zeta");
        $this->assertIsBool($result, "Incorrect result");
        $this->assertEquals($result, false);
        $result = vault::isDir("/data/zopa/mopa");
        $this->assertEquals($result, true);
        $result = vault::isDir("/datata");
        $this->assertEquals($result, true);
    }
    
    /**
     * Test delete value
     */
    public function testDeleteValue()
    {
        $result = vault::delete("/data/zopa/mopa/zeta/");
        $this->assertIsArray($result, "Incorrect result");
        $this->assertEquals($result['result'], true);
    }
}
