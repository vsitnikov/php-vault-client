<?php
try {
    $global_options = [];
    $file = null;
    if (file_exists(__DIR__."/init_options.json"))
        $filename = "init_options";
    elseif (file_exists(__DIR__."/init_options_dist.json"))
        $filename = "init_options_dist";
    if (!is_null($filename))
        $global_options = json_decode(file_get_contents(__DIR__."/{$filename}.json"), true);
} catch (Exception $e) {
}
